# Laravel Routes

## objective

Easily organize routes in project by feature.

## Get started

### Install

```bash
composer config repositories.gitlab.com/pecqueurs-public composer https://gitlab.com/api/v4/group/53853709/-/packages/composer/

composer require pecqueurs/laravel-middleware-json
```


### Usage

```php
// Json response format
Route::middleware('json-response')->group(function() {
    // Routes
});
```
