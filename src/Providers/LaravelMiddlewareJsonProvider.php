<?php

namespace PecqueurS\LaravelMiddleware\Json\Providers;

use Illuminate\Support\ServiceProvider;
use PecqueurS\LaravelMiddleware\Json\Middleware\ForceJsonResponse;

class LaravelMiddlewareJsonProvider extends ServiceProvider
{
    public function register()
    {
        app('router')->aliasMiddleware('json-response', ForceJsonResponse::class);
    }
}
